# <%= appTplName %>

This project is generated with [yo angularjs generator](https://github.com/joelchu/generator-angularjs)
version <%= pkg.version %>.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.


## Need help?

Visit [NewB](http://newb.im).

2015
